package ru.java.rush.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import ru.java.rush.entities.FruitEntity;

import java.util.List;

@Repository
public interface FruitRepository extends JpaRepository<FruitEntity, Integer> {

    List<FruitEntity> findByProviderCodeBetween(Integer from, Integer to);

    List<FruitEntity> countFruitEntityByFruitName(String name);


    @Query("select f.fruitName, p.providerName from  FruitEntity f left join ProviderEntity p on f.providerCode = p.id")
    List<String> joinSting();

    @Query(
            value = "select fruit_table.fruit_name, provider_table.provider_name from  fruit_table  join provider_table on fruit_table.provider_code = provider_table.id_provider",
            nativeQuery = true)
    List<String> joinProvider();

    @Query("select f from  FruitEntity f  join ProviderEntity p on f.providerCode = p.id")
    List<FruitEntity> joinFruit();
}
